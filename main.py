# Import Modules
import os
import socket
from requests import get

# Ask for bukkit
os.system("clear")
print("[AVAIABLE MODES]")
print("1: Vanilla")
print("2: Bukkit")
#print("3: Forge Server")
mode = input("What mode do you want? > ")

# Selects different files
if mode == "1":
    os.chdir("/home/mcss/")
    os.system("sudo python3 vanilla.py")

elif mode == "2":
    os.chdir("/home/mcss/")
    os.system("sudo python3 bukkit.py")

elif mode == "3":
    os.system("clear")
    print("Not supported yet...")

else:
    print("Default: Vanilla")
    os.chdir("/home/mcss/")
    os.system("sudo python3 vanilla.py")
