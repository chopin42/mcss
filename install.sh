# Installing Dependencies
sudo apt install -y python3.7 git openjdk-11-jre miniupnpc

# Configure the editor variable
export EDITOR=micro

if cd "/home/mcss"
then
	echo "Path is correct"
else
	sudo cp -r $(pwd) /home
	cd /home
fi

# Delete the BuildTools.jar file into /home/MCSS

# Copy to bin
sudo cp /home/mcss/bin/mcss /usr/bin
sudo cp /home/mcss/bin/mcss-update /usr/bin
sudo cp /home/mcss/bin/mcss-remove /usr/bin
echo "Bin files have been moved"

# Install BuildTools
cd /home/mcss/bukkit

file="/home/mcss/bukkit/BuildTools.jar"
if [ -f "$file" ]
then
	echo "$file found."
else
	sudo wget https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar
	echo "File downloaded"
fi

# Get authorisations
chmod 777 /usr/bin/mcss
chmod 777 /usr/bin/mcss-remove
chmod 777 /usr/bin/mcss-update
chmod 777 -R /home/mcss
echo "Authorizations granted"

echo "Done."
