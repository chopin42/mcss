import os, socket
from requests import get

def getIp():
    import socket
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("google.com",80))
    out = (s.getsockname()[0])
    s.close()
    return out

def getPublicIp():
    public = get('https://api.ipify.org').text
    return public

def genFile(version, path):
    os.system(f"java -jar {path}/BuildTools.jar --rev {version}")
    os.system(f"mv spigot-{version}.jar craftbukkit.jar")

def runServer(path, run):
    private = getIp()
    public = getPublicIp()
    os.system(f"upnpc -a {private} 25565 25565 TCP")
    print(f"THE SERVER IS RUNNING, YOUR IP IS {public}!")
    os.system(run)
    os.system("upnpc -d 25565 TCP")	
