import os
import socket
from requests import get
from modules import *

# Get inputs
name = input("What's your server name > ")
path = f"/home/mcss/servers/{name}"
run = f"chmod +x {path}/server.jar && java -jar {path}/server.jar nogui"
editor = "nano"

# Is the server exists?
if os.path.isdir(path) == True:
    runServer(path, run)
    exit()
else:
    # Creating the folder
    os.system("mkdir /home/mcss/servers")
    os.system(f"mkdir {path}")
    os.chdir(path)

    # Get the files
    os.system(f"cp -r /home/mcss/vanilla/* {path}")

    # Get the server
    url = input("Paste the URL of the file > ")
    os.system(f"sudo wget {url} {path}/server.jar")

    # Run the server
    runServer(path, run)

    # Edit server.properties
    next = input("Do you want to modify properties [Y, n] > ")
    if next == 'Y' or next == 'y':
        os.system(f"{editor} {path}/server.properties")
        input("When you finished, press enter...")

    # Run the server
    runServer(path, run)
