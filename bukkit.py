import os, socket
from requests import get
from modules import *

# Get the vars
name = input("What's your server's name > ")
path = f"/home/mcss/servers/{name}"
run = f"chmod +x {path}/craftbukkit.sh && sudo sh {path}/craftbukkit.sh"
version = input("What version do you want to get > ")
editor = "nano"

# Is the server exists?
if os.path.isdir(path) == True:
    runServer(path, run)
    exit()
else:
    # Creating the folder
    os.system("mkdir /home/mcss/servers")
    os.system(f"mkdir {path}")
    os.chdir(path)

    # Get the files
    os.system(f"cp -r /home/mcss/bukkit/* {path}")

    # Get the version file
    genFile(version, path)
    runServer(path, run)

    next = input("Do you want to modify properties [Y, n] > ")
    if next == "Y" or next == "y":
        os.system(f"{editor} {path}/server.properties")
        input("When you finished, press enter...")

    runServer(path, run)
