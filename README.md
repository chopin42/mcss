# MCSS (MineCraft Server Setup) YNH edition
How to create a server very easy without following tutorials? This is **MCSS**.
Works with Python on Linux (but will be expended on every platforms), create a Minecraft server for you and your friends!

> Warning : This app is under heavy developpement! Many features are buggy and doesn't work. Only Bukkit is available for the moment.

This is the YuNoHost edition of this package. This is in early version so all the installation must be done by hand. This is also a repository for Debian and Debian-based servers.

## Install

### Install on Linux / install from source on a server
To install this app on Linux just run the following commands:

```shell
git clone https://gitea.com/chopin42/mcss.git
cd mcss
sudo ./install.sh
```

### Install on YuNoHost
All the instructions about the package are here: https://gitea.com/chopin42/MCSS_ynh

> Warning the Yunohost repository is not up to date yet.

### Windows
Not yet... Please wait!

## Usage
To use it just run the command: `mcss`

## Troubleshooting

### Bukkit's compiler doesn't work and the server doesn't start

If the bukkit server doesn't start and the process of creating the version have been too short (should take a few minutes) then that probably means
that you don't have the correct java version. Only Java 8 is supported by Spigot and Craftbukkit. If you have a higher version, like Java 11, you will
need to downgrade.

If you installed Java using APT, that's how you can do it:

```shell
sudo update-alternatives --config java
```

After running this command, you will just have to select the line that contains "Java8".

Example:

```shell
There are 2 choices for the alternative java (providing /usr/bin/java).

Selection    Path                                            Priority   Status

* 0            /usr/lib/jvm/java-9-openjdk-amd64/bin/java       1500      auto mode
  1            /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java   1500      manual mode
  2            /usr/lib/jvm/java-9-openjdk-amd64/bin/java       1500      manual mode
```

In this case you would need to select 1. 

## Contribute
Of course you can contribute to this project, to make this just read the [CONTRIBUTING.md](opensource/CONTRIBUTING.md) file.

## LICENSE
This project is run by GNU public license, see [LICENSE.md](opensource/LICENSE.md) to learn more about it...
